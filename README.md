LOUIS VUITTON - HACKATHON 2018
=========================


## Description of the project :

- Build the ultimate search engine for the retailler
- See more : https://unlocksearchpower.louisvuitton.com/?utm_source=bma&utm_medium=1to142&utm_campaign=unlocksearchpower

## Team 
 - Yixuan
 - Naceur : http://naceur-abdeljalil.com

## Project's Goals and objectives

The goal is to propose a solution for an utlimate searchengine combining all intra LV applicaton
to assist the retailler to have a quick access to the right information.
   
    
## Technologies :
    - ElasticSearch
    - Kabana
    - NodeJs
    - Angular
    - Django/flusk
    - Docker
 
## Screenshots
## Install the development environment

Get the source:

```bash
git clone https://me-me@bitbucket.org/me-me/louis-vuitton_hackathon2018.git
```

Edit your `/etc/hosts` file:

```
127.0.0.1   app.kabana.local
```

### Requirements
- At least 4G RAM
- Windows and Mac users must configure their Docker virtual machine to have more than the default 2 GiB of RAM
- Docker version 17.12.1-ce or plus

Build the environment:

```bash
# Use your Bitbucket credentials to login in the Docker private registry for the project.
docker-compose -f setup.yml up
```
P.S: The build may take some time don't worry be happy and grab a cup of tea :)

Start your containers
```bash
# Launch thte docker containers
docker-compose up
```

Containers number : Nine (09)

Containners list 

```bash
# Containers list
docker.elastic.co/beats/auditbeat:6.3.0               "/usr/local/bin/dock…"                                         auditbeat
docker.elastic.co/beats/packetbeat:6.3.0              "/usr/local/bin/dock…"                                         packetbeat
docker.elastic.co/apm/apm-server:6.3.0                "/usr/local/bin/dock…"      0.0.0.0:8200->8200/tcp             apm_server
docker.elastic.co/beats/heartbeat:6.3.0               "/usr/local/bin/dock…"                                         heartbeat
docker.elastic.co/beats/filebeat:6.3.0                "/usr/local/bin/dock…"                                         filebeat
docker.elastic.co/beats/metricbeat:6.3.0              "/usr/local/bin/dock…"                                         metricbeat
docker.elastic.co/kibana/kibana:6.3.0                 "/usr/local/bin/kiba…"      0.0.0.0:5601->5601/tcp             kibana
docker.elastic.co/logstash/logstash:6.3.0             "/usr/local/bin/dock…"      5044/tcp, 9600/tcp                 logstash
docker.elastic.co/elasticsearch/elasticsearch:6.3.0   "/usr/local/bin/dock…"      0.0.0.0:9200->9200/tcp, 9300/tcp   elasticsearch

```

Connect to kaban : 
```bash.
app.kabana.local:5601
```

kaban credentials : 
```bash.
Username : elastic
password : you will recieve it within the build in your terminal
```

NOTE: Elasticsearch is now setup with self-signed certs.

### Help

To generate the bundler in app folder :
```bash
sudo ./node_modules/.bin/webpack
```

Start webpack in watch mode
```bash
# Install all dependencies.
sudo ./node_modules/.bin/webpack --watch
```

Stop and remove all containers

```bash
docker stop $(docker ps -a -q)
```

Connect to a container via bash (get the container name you want to connect to via command `docker ps`)
```bash
docker exec -ti containername bash
```

Execute a command directly in a container without connecting in bash (get the container name you want to connect to via command `docker ps`)

```bash
docker exec -i containername yourcommand
```

Delete all images 

```bash
docker rmi -f $(docker images -q)
```

Show inages 

```bash
docker images
```